/*----Wprowadzenie danych----*/
EXECUTE add_all 'BRAND1','Modele','Typ1','Dodadtkowe cos ponadto', 
				'Autor1',5,'1999-01-08', 'Review_Text',
					1,2,'Pros','Cons',true,'Recommend';

EXECUTE add_all 'BRAND2','Modele','Typ1','Dodadtkowe cos ponadto', 
				'Autor2',5,'1999-01-08', 'Review_Text',
					1,2,'Pros','Cons',true,'Recommend';

EXECUTE add_all 'BRAND1','Modele','Typ1','Dodadtkowe cos ponadto', 
				'Autor2',5,'1999-01-08', 'Review_Text',
					1,2,'Pros','Cons',true,'Recommend';
EXECUTE add_all 'BRAND2','Modele','Typ1','Dodadtkowe cos ponadto', 
				'Autor1',5,'1999-01-08', 'Review_Text',
					1,2,'Pros','Cons',true,'Recommend';
/*----Sprawdzanie----*/
SELECT *from PRODUCTS;
SELECT *from REVIEWS;
SELECT *from PRODUCTS_REVIEWS;
SELECT *from DELETEREVIEWS;
SELECT *from  LOGCHANCHE;

SELECT id_pr_rev,brand,model,author,review_data,review_text 
from products_reviews
LEFT JOIN products ON products_reviews.id_product = products.id_product
LEFT JOIN reviews ON products_reviews.id_review = reviews.id_review;
--Lub tylko gdy stworzono procedure myJux
EXECUTE myJux;
/*-------------UPDATE--------------------*/
UPDATE REVIEWS SET STARS_SCORE=6, PROS='ChangeIsNoTGood', REVIEW_DATA=GETDATE() WHERE ID_REVIEW=2; 
select * from REVIEWS ORDER BY ID_REVIEW;
select * from  LOGCHANCHE;
/*-------------DELETE--------------------*/
ENABLE TRIGGER delete_rev ON REVIEWS;
DELETE FROM REVIEWS WHERE ID_REVIEW=2;
SELECT * FROM DELETEREVIEWS;
SELECT *from PRODUCTS_REVIEWS;
SELECT *from REVIEWS;

/*-------------Reset--------------------*/
EXECUTE clean_my_database;
/*----R�cznie-----*/
DISABLE TRIGGER delete_rev ON REVIEWS;
Delete from PRODUCTS_REVIEWS;
Delete from PRODUCTS;
Delete from REVIEWS;
Delete from DELETEREVIEWS;
Delete from LOGCHANCHE;
DBCC CHECKIDENT ('PRODUCTS_REVIEWS', RESEED, 0);
DBCC CHECKIDENT ('PRODUCTS', RESEED, 0);
DBCC CHECKIDENT ('REVIEWS', RESEED, 0);