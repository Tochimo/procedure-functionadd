CREATE OR REPLACE  package jux_pack is
type jux is record(
      BRAND_C VARCHAR(100),MODEL_C  VARCHAR(100),
      AUTHOR_C VARCHAR(25),DATA_C DATE,
      REVIEW_C VARCHAR(2000));
type tab_jux is table of jux;
end;

create or replace FUNCTION myJux
RETURN jux_pack.tab_jux PIPELINED is
CURSOR cur_jux IS 
  SELECT brand,model,author,review_data,review_text from PRODUCTS_REVIEWS
	 LEFT JOIN PRODUCTS ON PRODUCTS_REVIEWS.id_product = PRODUCTS.id_product
	 LEFT JOIN REVIEWS ON PRODUCTS_REVIEWS.id_review = REVIEWS.id_review;
jux_t jux_pack.jux;
done INT DEFAULT 0;
BEGIN
SELECT Max(ID_PR_REV) INTO done from PRODUCTS_REVIEWS;
OPEN cur_jux;
WHILE done > 0 LOOP
FETCH cur_jux  INTO jux_t;
done:= done-1;
pipe row(jux_t);
    END LOOP;
     CLOSE cur_jux;
RETURN;
END;


 select * from table(myJux);
 
 
 