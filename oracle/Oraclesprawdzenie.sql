/*----Wprowadzenie danych----*/
call  add_all('BRAND1','Modele','Typ1','Dodadtkowe cos ponadto', 'Autor1',5,'1999-01-08', 'Review_Text',1,2,'Pros','Cons',1,'Recommend');
call add_all('BRAND2','Modele','Typ1','Dodadtkowe cos ponadto', 'Autor2',5,'1999-01-08', 'Review_Text',1,2,'Pros','Cons',1,'Recommend');
call add_all('BRAND1','Modele','Typ1','Dodadtkowe cos ponadto', 'Autor2',5,'1999-01-08', 'Review_Text',1,2,'Pros','Cons',1,'Recommend');
call add_all('BRAND2','Modele','Typ1','Dodadtkowe cos ponadto', 'Autor1',5,'1999-01-08', 'Review_Text',1,2,'Pros','Cons',1,'Recommend');
/*----Sprawdzanie----*/
SELECT *from PRODUCTS;
SELECT *from REVIEWS;
SELECT *from PRODUCTS_REVIEWS; 
SELECT id_pr_rev,trim(brand),model,author,review_data,trim(review_text) 
from products_reviews
LEFT JOIN products ON products_reviews.id_product = products.id_product
LEFT JOIN reviews ON products_reviews.id_review = reviews.id_review;
--Lub tylko gdy u�ytko funkcji w Oraclecursor
select * from table(myJux);
/*-------------UPDATE--------------------*/
UPDATE REVIEWS SET STARS_SCORE=6, PROS='ChangeIsNoTGood', REVIEW_DATA=SYSDATE WHERE ID_REVIEW=2; 
select * from REVIEWS ORDER BY ID_REVIEW;
select * from  LOGCHANCHE;
/*-------------DELETE--------------------*/
ALTER TRIGGER delete_rev Enable;
DELETE FROM REVIEWS WHERE ID_REVIEW=1;
SELECT * FROM DELETEREVIEWS;
SELECT *from PRODUCTS_REVIEWS;
/*----czyszcenie----*/
execute clean_my_database();
/*----Lub r�cznie----*/
ALTER TRIGGER delete_rev DISABLE;
Delete from PRODUCTS_REVIEWS;
Delete from PRODUCTS;
Delete from REVIEWS;
Delete from DELETEREVIEWS;
Delete from LOGCHANCHE;
EXECUTE reset_seq('products_seq');
EXECUTE reset_seq('reviews_seq');
EXECUTE reset_seq('products_reviews_seq');
EXECUTE reset_seq('logchange_seq');
EXECUTE reset_seq('jux_seq');


 
 
 

 