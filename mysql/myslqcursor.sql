--dodanie sekwencji
INSERT INTO sequence VALUES ('JuxSeq',1,1);
----Funkcja globalna resetujaca sekwencje
DELIMITER $$
CREATE PROCEDURE dev.resetJuxSeq()
SQL SECURITY DEFINER
BEGIN
Update sequence SET current_value=1 where name='JuxSeq';
 END $$
 DELIMITER ;
---ustawinia działalności trygara globalnego. 
 SET GLOBAL init_connect="CALL dev.resetJuxSeq()";
--Procedura
DELIMITER $$
 CREATE PROCEDURE myJux()
 BEGIN
     DECLARE BRAND_C VARCHAR(100);
     DECLARE MODEL_C  VARCHAR(100);
     DECLARE AUTHOR_C VARCHAR(25);
     DECLARE DATA_C DATE;
	 DECLARE REVIEW_C VARCHAR (2000);
	 DECLARE done INT DEFAULT 0;
     DECLARE cur_jux CURSOR FOR 
	 SELECT trim(brand),model,author,review_data,trim(review_text) from PRODUCTS_REVIEWS
	 LEFT JOIN PRODUCTS ON PRODUCTS_REVIEWS.id_product = PRODUCTS.id_product
	 LEFT JOIN REVIEWS ON PRODUCTS_REVIEWS.id_review = REVIEWS.id_review;
 
    
	 SET @done=(SELECT Max(ID_PR_REV) from PRODUCTS_REVIEWS);
 
     CREATE TEMPORARY TABLE IF NOT EXISTS JUXTAPOSITION
	 (ID_JUX SERIAL PRIMARY KEY,
	 BRAND_JUX VARCHAR(100),
	 MODEL_JUX VARCHAR(100), 
	 AUTHOR_JUX VARCHAR(25),
	 DATA_JUX DATE,
	 REVIEW_JUXT VARCHAR (2000));
     OPEN cur_jux;
      WHILE @done > 0 DO
 
       FETCH cur_jux INTO BRAND_C,MODEL_C,AUTHOR_C,DATA_C,REVIEW_C;
 
       INSERT INTO JUXTAPOSITION (ID_JUX,BRAND_JUX,MODEL_JUX,AUTHOR_JUX,DATA_JUX,REVIEW_JUXT)
           VALUES (nextval('JuxSeq'), BRAND_C,MODEL_C,AUTHOR_C,DATA_C,REVIEW_C );
	
	SET @done= @done-1;
	END WHILE;
     CLOSE cur_jux;
     /* Print out the changed salaries*/
 
     SELECT * FROM JUXTAPOSITION
     ORDER BY ID_JUX;
 END $$
 DELIMITER ;
--Wywołąnie
call myJux();




