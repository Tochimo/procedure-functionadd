------Trywialna sekwencja
CREATE TABLE Tests (
ID_PRODUCT  int PRIMARY KEY AUTO_INCREMENT,
Value1 varchar(25),
Value2 varchar(100)
);
----Tworzenie sekwencji  ;
 CREATE TABLE sequence (
name              VARCHAR(50) NOT NULL,
current_value INT NOT NULL,
increment       INT NOT NULL DEFAULT 1,
PRIMARY KEY (name)
) ENGINE=InnoDB;

INSERT INTO sequence VALUES ('TestSeq',1,1);

Drop FUNCTION currval;

DELIMITER $
CREATE FUNCTION nextval (seq_name VARCHAR(255))
RETURNS INTEGER
BEGIN
  DECLARE value INTEGER;
  SET value = 0;
  SELECT current_value INTO value FROM sequence WHERE name = seq_name;
  Update sequence set  current_value=current_value+ increment  WHERE name = seq_name;
  RETURN value;
END$
DELIMITER;
  
Insert into Tests values(nextval('TestSeq'),'Jeden','Dwa');
Select *from Tests;
Select *from sequence;
--reset
--Delete from sequence;
-- Update sequence set  current_value=1  WHERE name = 'TestSeq';