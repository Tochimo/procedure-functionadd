 KEYSPACE
 CREATE KEYSPACE dev WITH replication = {'class':'SimpleStrategy', 'replication_factor' : 3};
 use dev;
 --Tabela.
 CREATE TABLE wariors(
   war_id int,
   war_name text,
   war_race text,
   war_weapon text,
   war_mount text,
   war_magic text,
  weapon_strengt bigint,
    PRIMARY KEY((war_id),war_name,war_race)
   );
--Wtsawianie 
 INSERT INTO wariors  (war_id, war_name, war_race, war_weapon, weapon_strengt, war_magic) VALUES (1,'Shaman', 'Ogr', 'Mace',15,'UnknowBe');
 INSERT INTO wariors  (war_id, war_name, war_race, war_weapon, weapon_strengt, war_mount) VALUES (2,'Barbarian', 'Orc', 'Hammer',15,'Wolf');
 INSERT INTO wariors  (war_id, war_name, war_race, war_weapon, weapon_strengt) VALUES (3,'Archer', 'Orc', 'Bow',10);
 INSERT INTO wariors  (war_id, war_name, war_race, war_weapon, weapon_strengt) VALUES (4,'Assain', 'Goblin', 'Knief',5);
  select * from wariors;

 --kopia danych do pliku
 COPY wariors   TO 'temp.txt'  (war_id, war_name, war_race, war_weapon)

--Funkcja zliczająca makymalną siłe poszczególnych ras.
 CREATE OR REPLACE FUNCTION cumulateRace(state map<text,bigint>, race text, strengt bigint)
RETURNS NULL ON NULL INPUT
RETURNS map<text,bigint>
LANGUAGE java
AS '
if(state.containsKey(race)) {
  state.put(race,  (Long)state.get(race) + strengt); 
} else {
  state.put(race, strengt);
}
return state;
'; 

--Funkcja agregująca. 
CREATE OR REPLACE AGGREGATE groupCountByRace(text, bigint)
SFUNC cumulateRace
STYPE map<text,bigint>
INITCOND {};

SELECT groupCountByRace(war_race,weapon_strengt) 
FROM wariors 
WHERE war_id IN(1,2,3,4);
---usuwanie
drop REPLACE FUNCTION cumulateRace( map<text,bigint>,  text,  bigint) ;
drop AGGREGATE groupCountByRace(text, bigint);
---Podglad funkcji
SELECT function_name, signature FROM system.schema_functions;
---Podglad agregacji
SELECT aggregate_name,signature FROM system.schema_aggregates;
---Podglad typów. Nie tworzono żadnych.
SELECT * FROM system.schema_usertypes;
