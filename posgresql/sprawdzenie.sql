/*----Wprowadzenie danych----*/
SELECT ADD_ALL('BRAND1','Modele','Typ1','Dodadtkowe cos ponadto', 'Autor1',5,'1999-01-08', 'Review_Text',1,2,'Pros','Cons',true,'Recommend');
SELECT ADD_ALL('BRAND2','Modele','Typ1','Dodadtkowe cos ponadto', 'Autor2',5,'1999-01-08', 'Review_Text',1,2,'Pros','Cons',true,'Recommend');
SELECT ADD_ALL('BRAND1','Modele','Typ1','Dodadtkowe cos ponadto', 'Autor2',5,'1999-01-08', 'Review_Text',1,2,'Pros','Cons',true,'Recommend');
SELECT ADD_ALL('BRAND2','Modele','Typ1','Dodadtkowe cos ponadto', 'Autor1',5,'1999-01-08', 'Review_Text',1,2,'Pros','Cons',true,'Recommend');
/*----Sprawdzanie----*/
SELECT *from PRODUCTS;
SELECT *from REVIEWS;
SELECT *from PRODUCTS_REVIEWS;
SELECT id_pr_rev,trim(brand),model,author,review_data,trim(review_text) 
from products_reviews
LEFT JOIN products ON products_reviews.id_product = products.id_product
LEFT JOIN reviews ON products_reviews.id_review = reviews.id_review;
--Lub tylko gdy użytko funkcji w prostreslqcursor
SELECT myJux();
/*-------------UPDATE--------------------*/
UPDATE REVIEWS SET STARS_SCORE=6, PROS='ChangeIsNoTGood', REVIEW_DATA=NOW() WHERE ID_REVIEW=2; 
select * from REVIEWS ORDER BY ID_REVIEW;
select * from  LOGCHANCHE;
/*-------------DELETE--------------------*/
ALTER TABLE REVIEWS ENABLE TRIGGER delete_rev;
DELETE FROM REVIEWS WHERE ID_REVIEW=2;
SELECT * FROM DELETEREVIEWS;
SELECT *from PRODUCTS_REVIEWS;
/*----czyszcenie----*/
SELECT clean_my_database();
/*----Lub ręcznie----*/
Drop TRIGGER delete_rev ON REVIEWS  ;
Delete from PRODUCTS_REVIEWS;
Delete from PRODUCTS;
Delete from REVIEWS;
Delete from DELETEREVIEWS;
Delete from LOGCHANCHE;
/*----ustawianie na 0----*/
SELECT setval('products_id_product_seq', 0);
SELECT setval('reviews_id_review_seq', 0);
SELECT setval('products_reviews_id_pr_rev_seq', 0);

